# -*- coding: utf-8 -*-
#Pre-convert program

rf = open("american-english","r")
wf = open("sorted-american-english", "w")

for row in rf:
	if row.find("'") == -1:
		raw = row.rstrip("\n")
		row = raw.lower()
		li = list(row)
		li.sort()
		text = "".join(li)
		wf.write(text+","+raw+"\n")
		print text
rf.close()
wf.close()