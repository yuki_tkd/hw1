# -*- coding: utf-8 -*-
#Anagram

from datetime import datetime
from time import sleep

#文字列の長さで比較するための関数
def cmp_len(s1, s2):
	ss1 = str(s1).replace("0", "")
	ss2 = str(s2).replace("0", "")
	return cmp(len(ss1), len(ss2))

#入力されたリストの部分集合全体を求める
def solve(input):
	n = len(input)
	pattern ,result = [], []
	#並び替え成功した単語数
	Anscnt = 0

	#使う使わないを並び替え
	for i in range(1, pow(2, n)):
		s = "%0" + str(n) + "d"
		#2進数表現して、0,1をそれぞれ使う、使わないに置き換える
		pattern.append(s % int(format(pow(2, n)-i, 'b')))


	#1が多い順に並び替え
	pattern.sort(cmp=cmp_len,reverse=True)

	for i in pattern:
		s = ""
		for j, v in enumerate(list(i)[::-1]):
			if v == "1" :
				s += input[j]

		if s not in result:
			result.append(s)

			#sに部分集合が大きい順で入っている
			if dictword.has_key(s) == True:
				#リストの要素数のやつに変えても動くはず
				for i,words in enumerate(dictword[s]):
					if len(s) == n:
						print "Perfect Answer = " + dictword[s][i]
					else:
						print "Answer = " + dictword[s][i]
					Anscnt+=1

		#候補が10個以上出たらそこで打ち切り
		if Anscnt > 10:
			return
	return


#並び替え用辞書インポート
cf = open("sorted-american-english", 'r')
dictword={}
tmp = [e for e in cf.readlines()]
#辞書(ハッシュテーブル)の作成　
for e in tmp:
	#カンマ区切りファイルの読み込み
	temp = e.split(",")
	#改行文字取り除く
	temp[1] = temp[1].rstrip("\n")
	#新規登録
	if dictword.has_key(temp[0]) == False:
		tlist = [temp[1]]
		dictword[temp[0]] = tlist
	#同一キーが存在する場合
	else:
		tlist = dictword[temp[0]]
		tlist.append(temp[1])
		dictword[temp[0]] = tlist


while True:
	#検索文字読み取り
	inputchar = raw_input("\ninput > ")
	#検索文字前処理
	#小文字化
	inputchar = inputchar.lower()
	li = list(inputchar)
	#並び替え
	li.sort()

	#アナグラム解析
	solve(li)

